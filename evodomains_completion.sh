#!/usr/bin/env bash

_evodomains_dynamic_completion() {
    local action=""
    for w in "${COMP_WORDS[@]}"; do
        case "$w" in
            list|check-dns)
                action="${w}"
                ;;
        esac
    done

    local prev="${COMP_WORDS[$COMP_CWORD - 1]}"
    local words="--output --search --numeric --verbose --no-warnings --no-pager --non-interactive --help"

    if [ -z "${action}" ]; then
        words="check-dns list ${words}"
    fi

    if [ "${prev}" == "--output" ] || [ "${prev}" == "-o" ]; then
        if [ "${action}" == "list" ]; then
            words="human json"
        else
            words="human json nrpe"
        fi
    fi

    if [ "${prev}" == "--location" ] || [ "${prev}" == "-l" ]; then
        words="apache nginx certificates"
    fi

    # Avoid double
    opts=();
    for i in ${words}; do
        for j in "${COMP_WORDS[@]}"; do
            if [[ "$i" == "$j" ]]; then
                continue 2
            fi
        done
        opts+=("$i")
    done

    local cur=${COMP_WORDS[COMP_CWORD]};
    COMPREPLY=($(compgen -W "${opts[*]}" -- "${cur}"))
}

complete -F _evodomains_dynamic_completion evodomains

