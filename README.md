# Evodomains

Evodomains is a Python script to ease the management of a server's domains


## Usage

~~~
# evodomains -h
usage: evodomains [-h] [-d] [-n] [-o OUTPUT] [-s] [-v] ACTION

positional arguments:
  ACTION                Values: check-dns, list

optional arguments:
  -h, --help            show this help message and exit
  -d, --debug           Print debug to stderr and enable '--verbose'.
  -n, --numeric         Show only IPs, no reverse DNS.
  -o OUTPUT, --output OUTPUT
                        Output format. Values: 'human' (default), 'json', 'nrpe'
                        (only with 'check-dns' action)
  -s, --no-warnings     Silence warnings (useful for '--output json').
  -v, --verbose         With 'check-dns', print also OK domains. With '--output json', print details for each domain.
~~~


## Installation and update

### With Ansible

Use the role [`evodomains`](https://gitea.evolix.org/evolix/ansible-roles/src/branch/stable/evodomains) of [`ansible-role` repository](https://gitea.evolix.org/evolix/ansible-roles).


### Manually

Note : the instructions are the same for installation and update.

Clone the repo in the directory of your choice:

~~~
git clone https://gitea.evolix.org/evolix/evodomains.git
cd evodomains
git switch stable
~~~

Note : The stable branch points to the last release branch. It will follow the releases. If you want to stay on a specific release and only get this release fixes, switch to the release branch.

Execute as root:

~~~
cp ./evodomains.py /usr/local/bin/evodomains
chmod 700 /usr/local/bin/evodomains
if [ -d '/etc/bash_completion.d' ]; then
    cp ./evodomains_completion /etc/bash_completion.d/evodomains_completion
    chmod 644 /etc/bash_completion.d/evodomains_completion
fi

conf_path="/etc/evolinux/evodomains"
mkdir -p "${conf_path}"

for path in "${conf_path}/ignored_domains_check.list" "${conf_path}/included_domains_check.list" "${conf_path}/allowed_ips_check.list" "${conf_path}/wildcard_replacements"; do
    touch "${path}"
done
~~~


## Configuration

### Ignore domains

`evodomains` looks for domains in Apache, Nginx and some default SSL certificate paths (Let's Encrypt

To exclude some of them from `evodomains check-dns` command output, add them to `/etc/evolinux/evodomains/ignored_domains_check.list`.

Format: one domain per line, regex and wildcards not supported.


### Add domains

`evodomains` looks for domains in Apache, Nginx and some default SSL certificate paths (Let's Encrypt
 `etc/ssl/certs`).

For `evodomains` to take into account more domains, add them to `/etc/evolinux/evodomains/included_domains_check.list`

Format: one domain per line, regex and wildcards not supported.


### Allow domains to point to external IPs

By default, `evodomains check-dns` allows only hostmane IPs. All other are reported as wrong DNS pointing.

To allow more IPs, add them to `/etc/evolinux/evodomains/allowed_ips_check.list`

Format: one IP per line, regex and wildcards not supported.


### Define a wildcard replacement domain to check

By default, wildcards like `*.example.com` are replaced with `www.example.com`.

If you want `evodomains check-dns` to check another domain for a wildcard, add it to `/etc/evolinux/evodomains/wildcard_replacements`.

Format: on pair per line, like this : `WILDCARD_DOMAIN REPLACEMENT_DOMAIN`.


## Packaging

Clone the repository.

Warning: the package will be built into the parent directory.

Execute:

~~~
$ git switch debian
$ debuild -us -uc
~~~

Then you can use the package `../evodomains_VERSION_all.deb`
