#!/usr/bin/python3
#
# Evodomains is a Python script to ease the management of a server's domains and their certificates.
#
# Execute 'evodomains --help' for usage.
#
# Features:
# - Search for domains in Apache and Nginx configuration, and in TLS$ certificates.
# - Check domains DNS (IPv4 only).
# - Check domains TLS certificates (IPv4 only).
#
# Roadmap :
# - Check SSL certificates usage and expiration date.
# - Support search in HaProxy certificates
#
# Minimal requirement: Debian 9
# Author: Will


# Standard library

import argparse, os, sys, shutil, tempfile, glob
import socket, ipaddress
import subprocess, multiprocessing.pool, threading
import re, time, json, textwrap, operator
from datetime import datetime, timedelta
from typing import Type, List, Dict, Tuple, Callable
from enum import Enum
import pprint

# Dependencies

try:
    from tabulate import tabulate
except:
    print('Missing tabulate module. Please install python3-tabulate package.', file=sys.stderr)
    exit(1)

try:
    import cryptography
except:
    print('Missing cryptography module. Please install python3-cryptography package.', file=sys.stderr)
    exit(1)

cryptography_major_version = int(cryptography.__version__.split('.')[0])
cryptography_handles_verification = cryptography_major_version > 43
from cryptography import x509
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.backends import default_backend


"""
Global vars
"""

version = '0.1.1'
conf_dir_path = '/etc/evolinux/evodomains'
ignored_domains_file = 'ignored_domains_check.list'
included_domains_file = 'included_domains_check.list'
allowed_ips_file = 'allowed_ips_check.list'
wildcard_replacements_file = 'wildcard_replacements'
#haproxy_conf_path = '/etc/haproxy/haproxy.cfg'

# Regex

ip_regex = re.compile('^([0-9abcdef\.:]+)$')
domain_regex = re.compile('^(((?!-)[A-Za-z0-9\-\*]{1,63}(?<!-)\.)+[A-Za-z]{2,6})$')
reverse_regex = re.compile('^(((?!-)[A-Za-z0-9\-\*]{1,63}(?<!-)\.)+[A-Za-z]{2,6})\.$')
wildcard_regex = re.compile('^(\*\.((?!-)[A-Za-z0-9\-\*]{1,63}(?<!-)\.)+[A-Za-z]{2,6})$')
# Match a wildcard domain followed by a domain (space separated)
wildcard_replacement_conf_regex = re.compile('^(\*\.(?:(?!-)[\-A-Za-z0-9]{1,63}(?<!-)\.)+[A-Za-z]{2,6})\s+((?:(?!-)[\-A-Za-z0-9]{1,63}(?<!-)\.)+[A-Za-z]{2,6})$')
domain_cn_regex = re.compile('CN\s*=\s*(((?!-)[A-Za-z0-9\-\*]{1,63}(?<!-)\.)+[A-Za-z]{2,6})')
domain_san_regex = re.compile('DNS:(((?!-)[A-Za-z0-9\-\*]{1,63}(?<!-)\.)+[A-Za-z]{2,6})')

# Time to wait for DNS answer before considering a domain has timeout.
# Note: DNS check of all domains must be < 10s to avoid Icinga timeout.
DNS_timeout = 5

# Time delta to alert before certificate expiration
delta_warn_cert_expiration = timedelta(weeks=5)

# Dependencies (see check_dependencies())
pager = None
less_version = None
is_apache_mod_info = False


"""
Data classes
"""


class DomainSource:
    """ Abstract class to store the infos about where a domain was found.
    For inheritance only, this class mustn't be instantiated.
    Attributes:
        domain: the domain or subdomain
        roommates: other domains associated with domain in the same vhost or certificate
        source (suggested values):
            - 'apache', 'nginx', 'certbot', 'evoacme'
            - 'evodomains_conf' : from evodomains configuration (included_domains_check.list) on command-line
            - 'system_cert' : from /etc/ssl/certs/
            - 'custom_cert' : from --location option
        source_type: type of source ('conf_file', 'certificate', 'cli')
        path: conf file or certificate path where the domain was found
    """
    def __init__(self, domain: str, roommates: List[str], source: str, source_type: str, path: str):
        self.domain = domain
        self.roommates = roommates
        self.source = source
        self.type = source_type
        self.path = path

    def __str__(self):
        return str(self.__dict__)

    def __eq__(self, other):
        if not isinstance(other, DomainSource):
            return False

        return (self.domain == other.domain
                and self.source == other.source
                and self.type == other.type
                and self.path == other.path
                )


class EvodomainConfFileSource(DomainSource):
    """ DomainSource for domains from included_domains_file.
    """
    def __init__(self, domain: str, line_number: int, path: str = None):
        if not path:
            path = os.path.join(conf_dir_path, included_domains_file)
        self.line_numbers = [line_number]
        super().__init__(domain, [], 'evodomains', 'conf_file', path)


class EvodomainCommandLineSource(DomainSource):
    """ DomainSource for domains in command-line arguments.
    """
    def __init__(self, domain: str):
        super().__init__(domain, [], 'evodomains', 'cli', '')


class ApacheVhost:
    """ Represent an Apache vhost
    Used for to parse the output of 'apache2ctl -t -D DUMP_VHOSTS'
    """
    def __init__(self, servername: str, path: str, port: int, start_line: int):
       self.servername = servername
       self.path = path
       self.port = port
       self.start_line = start_line
       self.aliases = []

    def __repr__(self):
        return str(self.__dict__)


class WebSource(DomainSource):
    """ DomainSource for Apache and Nginx.
    For inheritance only, this class mustn't be instantiated.
    Attributes:
        line_numbers: list of line numbers of the occurences of domain in conf file
        ports: list of listening ports
        certificates: list of certificates paths
        intermediate_certificates: list of intermediate certificates paths (SSLCertificateChainFile deprecated in Apache, but still used)
        others: see DomainSource.
    """
    def __init__(self, domain: str, roommates: List[str], web_source: str, path: str):
        super().__init__(domain, roommates, web_source, 'conf_file', path)
        self.line_numbers = []
        self.ports = []
        self.certificates = []
        self.intermediate_certificates = []

    def add_line_number(self, line_number: int):
        if line_number not in self.line_numbers:
            self.line_numbers.append(line_number)

    def add_port(self, port: int):
        if port not in self.ports:
            self.ports.append(port)

    def add_certificate(self, cert_path):
        if cert_path not in self.certificates:
            self.certificates.append(cert_path)

    def add_intermediate_certificate(self, cert_path):
        if cert_path not in self.intermediate_certificates:
            self.intermediate_certificates.append(cert_path)

    def __eq__(self, other):
        if not isinstance(other, WebSource):
            return False

        return (super().__eq__(other)
                and sorted(self.line_numbers) == sorted(other.line_numbers)
                and sorted(self.ports) == sorted(other.ports)
                )

class ApacheSource(WebSource):
    """ WebSource for Apache.
    """
    def __init__(self, domain: str, roommates: List[str], path: str):
        super().__init__(domain, roommates, 'apache', path)
        self.line_numbers = []
        self.ports = []

    def __eq__(self, other):
        if not isinstance(other, ApacheSource):
            return False
        return (super().__eq__(other))


class NginxSource(WebSource):
    """ WebSource for Nginx.
    """
    def __init__(self, domain: str, roommates: List[str], path: str):
        super().__init__(domain, roommates, 'nginx', path)
        self.line_numbers = []
        self.ports = []

    def __eq__(self, other):
        if not isinstance(other, NginxSource):
            return False
        return (super().__eq__(other))


class OrderedEnum(Enum):
    """ Enum that can be compared with '<'.
    For sorting purposes.
    """
    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.value < other.value
        raise NotImplementedError


class CertExpirationStatus(OrderedEnum):
    """ DNS answer status
    Note : Numbers are also used for sorting.
    """
    EXPIRED = 1
    EXPIRES_SOON = 2
    OK = 3

    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.value < other.value
        raise NotImplementedError


class CertCheckResult:
    def __init__(self, expiration_status: CertExpirationStatus = CertExpirationStatus.OK):
        self.expiration_status = expiration_status
        self.comments = []

    def add_comment(self, comment: str):
        """ Argument 'comment' must be a simple sentence,
        without capital letter nor dot at the end.
        All comments will be concatenated, separated by commas.
        """
        self.comments.append(comment)


class Certificate:
    """ Represent a certificate.
    """
    def __init__(self, path: str, cns: List[str], sans: List[str], end_date: datetime):
        self.path = path
        self.cns = sorted(cns)
        self.sans = sorted(sans)
        self.end_date = end_date
        self.vhost_path = None
        self.cert_check_result = None

    def set_cert_check_result(self, cert_check_result: CertCheckResult):
        self.cert_check_result = cert_check_result


class CertificateSource(DomainSource):
    """ DomainSource for X.509 certificates.
    Attributes:
        cert_attributes: list of certificate attribute ('CN', 'SAN')
    """
    def __init__(self, domain: str, roommates: List[str], source: str, path: str):
        super().__init__(domain, roommates, source, 'certificate', path)
        self.cert_attributes = []

    def add_cert_attribute(self, cert_attribute: int):
        self.cert_attributes.append(cert_attribute)

    def __eq__(self, other):
        if not isinstance(other, ApacheSource):
            return False

        return (super().__eq__(other)
                and sorted(self.cert_attributes) == sorted(other.cert_attributes)
                )


#class HaProxySource(DomainSource):
#    """ DomainSource for HaProxy."""
#    def __init__(self, domain, source_type, path, line, port=None, cert_attributes=[]):
#        super().__init__(domain, 'haproxy', source_type, path, line, port, cert_attributes)


class DNSCheckStatus(OrderedEnum):
    """ DNS answer status.
    Note : Numbers are also used for sorting.
    """
    UNCHECKED = 1
    NO_DNS_RECORD = 2
    DNS_TIMEOUT = 3
    UNKNOWN_IPS = 4
    ERROR = 5
    OK = 6


class DNSCheckResult:
    def __init__(self):
        self.status = None
        self.allowed_ips = {}  # { IP: reverse, … }
        self.unknown_ips = {}  # idem
        self.comments = []

    def __eq__(self, other):
        """ Return True if other has the sames IPs and status.
        """
        if type(other) != type(self):
            return False

        return (set(other.allowed_ips) == set(self.allowed_ips)
            and set(other.unknown_ips) == set(self.unknown_ips)
            and other.status == self.status)

    def set_status(self, status: DNSCheckStatus):
        if not isinstance(status, DNSCheckStatus):
            raise ValueError('Unknown DNS status {}'.format(status))
        self.status = status

    def add_ip(self, ip: str, allowed: bool, reverse: str = None):
        if allowed:
            self.allowed_ips[ip] = reverse
        else:
            self.unknown_ips[ip] = reverse

    def add_comment(self, comment: str):
        """ Argument 'comment' must be a simple sentence,
        without capital letter nor dot at the end.
        All comments will be concatenated, separated by commas.
        """
        self.comments.append(comment)


class DomainSummary:
    """ Data structure that contains infos about a domain and its DNS test results.
    """
    def __init__(self, domain: str, replacement_domain: str = None):
        self.domain = domain
        self.replacement_domain = replacement_domain  # for wildcards
        self.sources = []
        self.dns_check_result = None

    def add_source(self, source: Type[DomainSource]):
        self.sources.append(source)

    def add_sources(self, sources: List[Type[DomainSource]]):
        self.sources.extend(sources)

    def set_dns_check_result(self, dns_check_result: DNSCheckResult):
        self.dns_check_result = dns_check_result


class CustomJSONEncoder(json.JSONEncoder):
    """Encode in JSON usual types and classes defined by evodomains.
    """
    def default(self, obj):
        if (isinstance(obj, DomainSummary) or isinstance(obj, DomainSource)
                                              or isinstance(obj, DNSCheckResult)
                                              or isinstance(obj, Certificate)
                                              or isinstance(obj, CertCheckResult)):
            # Remove None values
            d = { key:value for key, value in obj.__dict__.items() if value != None and value != [] }
            return d
        elif isinstance(obj, DNSCheckStatus):
            return obj.name
        elif isinstance(obj, datetime) or isinstance(obj, timedelta):
            return str(obj).split('.')[0]  # convert to string + remove millisecs
        else:
            return json.JSONEncoder.default(self, obj)



"""
General functions
"""


def main(argv):
    parse_arguments()
    check_dependencies()
    load_configuration()

    domains_summaries = list_domains(locations)

    if action == 'list':
        if output == 'nrpe':
            print_error_and_exit('Action \'list\' is not available for \'--output nrpe\'.')
        elif output == 'json':
            print_domains_json(domains_summaries)
        elif output == 'table':
            print_domains_table(domains_summaries)

    elif action == 'check-dns':
        if is_verbose and (output == 'json' or output == 'table'):
            print_allowed_ips()

        check_domains(domains_summaries)

        if output == 'nrpe':
            print_dns_check_nrpe(domains_summaries)
        elif output == 'json':
            print_dns_check_json(domains_summaries)
        elif output == 'table':
            print_dns_check_table(domains_summaries)

    elif action in ['check-certs', 'check-ssl']:
        certs = check_certificates(domains_summaries)

        if output == 'nrpe':
            print_certificates_check_nrpe(certs)
        elif output == 'json':
            print_certificates_check_json(certs)
        elif output == 'table':
            print_certificates_check_table(certs)


def parse_arguments():
    parser = argparse.ArgumentParser(prog='evodomains')
    parser.add_argument('action', metavar='ACTION', choices=['help','list', 'check-dns', 'check-certs', 'check-ssl'], default='help', help='Available actions: list: list the domains, check-dns: check the domains DNS, check-certs: check the TLS certificates, help: show this help')

    group = parser.add_mutually_exclusive_group()
    group.add_argument('domains', metavar='DOMAIN', nargs='*', default=[], help='Provide directly domain names for check-dns action. In this case, the domains search is skipped.')
    group.add_argument('-l', '--location', nargs='+', help='location(s) to search domains, space-separated, values can be \'apache\', \'nginx\' or \'certificates\' (all three by default), a directory path (certificates only, not recursive) or an enabled vhost name')

    parser.add_argument('-d', '--debug', action='store_true', help='print debug to stderr and enable --verbose')
    parser.add_argument('-n', '--numeric', action='store_true', help='show only IPs, no reverse DNS')
    parser.add_argument('--no-pager', action='store_true', help='do not pipe output into a pager')
    parser.add_argument('--non-interactive', action='store_true', help='run without confirmation messages and disable colors (implies --no-pager as well)')
    parser.add_argument('-o', '--output', default='table', help='output format, values: table (default), json, nrpe (only with check-dns action)')
    parser.add_argument('-q', '--no-warnings', action='store_true', help='quiet, suppress warnings (useful for --output json)')
    parser.add_argument('-v', '--verbose', action='store_true', help='with check-dns, print also OK domains; with --output json, print details for each domain')
    parser.add_argument('-V', '--version', action='version', version='%(prog)s {}'.format(version))

    args = parser.parse_args()

    if args.action == 'help':
        parser.print_help()
        exit(0)

    global action, action_domains, output, locations
    global is_debug, is_numeric, is_warning, is_verbose, is_pager, is_interactive

    # Booleans
    is_debug = args.debug
    is_numeric = args.numeric
    is_warning = not args.no_warnings
    is_verbose = True if is_debug else args.verbose
    is_pager = not args.no_pager
    if args.non_interactive:
        is_interactive = False
    else:
        is_interactive = sys.__stdin__.isatty() and sys.__stdout__.isatty()

    for arg, value in vars(args).items():
        print_debug('{} = {}'.format(arg, value))

    # Other args

    output = args.output
    if output not in ['table', 'json', 'nrpe']:
        err_msg = 'Unknown {} argument for --output option.'.format(output)
        print_error_and_exit(err_msg)

    action = args.action
    if action not in ['list', 'check-dns', 'check-certs', 'check-ssl']:
        print_error_and_exit('Unknown {} action, use -h option for help.'.format(action))
    action_domains = []
    for domain in args.domains:
        match = domain_regex.search(domain)
        if match and match.group(1):
            action_domains.append(match.group(1))
        else:
            print_error_and_exit('Argument {} is not a valid domain name.'.format(domain))

    if action_domains and action not in ['check-dns', 'check-certs', 'check-ssl']:
        print_error_and_exit('DOMAIN argument is only relevant with check-dns or check-certs ({}).'.format(', '.join(action_domains)))

    locations = args.location

    print_debug('---')
    print_debug('Action requested: {}.'.format(action))
    print_debug('Search domains in: {}.'.format(locations))
    print_debug('Output in {} format.'.format(output))
    print_debug('---')


def check_dependencies():
    global pager, less_version
    if 'PAGER' in os.environ and os.environ['PAGER']:
        pager = os.environ['PAGER']
    else:
        less_version_cmd = "less --version | head -n1 | awk '{print$2}'"
        stdout, _, _ = execute(less_version_cmd, shell=True)
        less_version = int(stdout[0])
        pager = 'less --quit-if-one-screen --chop-long-lines --RAW-CONTROL-CHARS'

    global is_apache_mod_info
    try:
        stdout, stderr, rc = execute('apache2ctl -t -D DUMP_MODULES')
        for line in stdout:
            if 'info_module' in line.split():
                is_apache_mod_info = True
                break
        if not is_apache_mod_info:
            print_warning('Apache mod_info is not enabled, please enable it with \'a2enmod info\'')
    except:
        pass

    # TODO: check socat for HaProxy


def load_configuration():
    # Create missing directories and files
    if not os.path.exists(conf_dir_path):
        os.makedirs(conf_dir_path, mode=0o755, exist_ok=True)

    # Load configuration in global variables
    global ignored_domains, evodomain_domains, allowed_ips, allowed_ip_reverses, wildcard_replacements
    ignored_domains = read_conf_file(conf_dir_path + '/' + ignored_domains_file)
    ignored_domains.append('_')
    wildcard_replacements = load_wildcard_replacements(conf_dir_path + '/' + wildcard_replacements_file)

    evodomain_domains_lines = read_conf_file(conf_dir_path + '/' + included_domains_file)
    evodomain_domains = [ (i+1, evodomain_domains_lines[i]) for i in range(len(evodomain_domains_lines)) ]

    allowed_ips = load_allowed_ips(conf_dir_path + '/' + allowed_ips_file)
    allowed_ip_reverses = {}  # dict: { key: ip, value: reverse }
    if not is_numeric:
        allowed_ip_reverses = query_ip_reverses(allowed_ips)

    # Add host IPs to allowed_ips and allowed_ip_reverses
    stdout, stderr, rc = execute('hostname -I')
    if stdout:
        host_ips = stdout[0].strip().split()
        allowed_ips.extend(host_ips)
        for ip in host_ips:
            allowed_ip_reverses[ip] = 'localhost'
    else:
        print_warning('Allow hostname IPs : command \'hostname -I\' returned no result.')


def load_allowed_ips(conf_path: str):
    """ Return the list of IPs the domains are allowed to point to in the configuration file.
    """
    allowed_ips = []
    conf_ips = read_conf_file(conf_path, ip_regex)
    for (ip,) in conf_ips:  # (ip,) unpacks a tuple of one element
        allowed_ips.append(ip)
    return allowed_ips


def load_wildcard_replacements(conf_path: str):
    """ Return a dict containing wildcard domains as keys, and replacement domains as values,
    from the configuration file.
    Example: {
        '*.evolix.com': 'evolix.com',
        '*.evolix.net': 'ssl.evolix.net',
        …
    }
    """
    replacements_tuples = read_conf_file(conf_path, wildcard_replacement_conf_regex)
    replacements = {}
    for (wildcard, replacement) in replacements_tuples:
        replacements[wildcard] = replacement
    return replacements



"""
Business logic : domains search
"""


class Location(Enum):
    APACHE = 'apache'
    NGINX = 'nginx'
    #HAPROXY = 'haproxy'
    CERTIFICATES = 'certificates'


def list_domains(locations: List = []):
    """ List domains from all sources.
    Return a dict { key: domain, value: DomainSummary object }
    """
    sources = []

    if action_domains:
        # Domains are provided in command-line arguments
        for domain in action_domains:
            source = EvodomainCommandLineSource(domain)
            if source not in sources:
                sources.append(source)

    else:
        if not locations:
            locations = [Location.APACHE.value, Location.NGINX.value, Location.CERTIFICATES.value] # default

            # Add domains given in evodomains configuration
            for (line_number, domain) in evodomain_domains:
                source = EvodomainConfFileSource(domain, line_number)
                if source not in sources:
                    sources.append(source)

        # Search around for domains
        for location in locations:
            if location == Location.APACHE.value:
                vhosts = parse_apache_dump_vhosts()
                sources += list_apache_domains(vhosts)
            elif location == Location.NGINX.value:
                sources += list_nginx_domains()
            elif location == Location.CERTIFICATES.value:
                sources += list_letsencrypt_domains()
                sources += list_certificates_domains('/etc/ssl/certs', 'system_cert')
            #elif location == Location.HAPROXY.value:
                #sources += list_haproxy_certificates_domains()
            elif os.path.isdir(location):
                sources += list_certificates_domains(location, 'custom_cert')
            else: # location == vhost ?
                sources += list_vhost_domains(location)

        if not sources:
            print_error_and_exit('No domain found.')

    summaries = {}
    for source in sources:
        if source.domain not in summaries:
            summaries[source.domain] = DomainSummary(source.domain, replace_wildcard(source.domain))
        summaries[source.domain].add_source(source)
    return summaries


def replace_wildcard(wildcard: str):
    """ Return a replacement domain for a wildcard domain.
    If configuration has no replacement, print a warning and replace *.DOMAIN by www.DOMAIN
    """
    wildcard_replacement = ''
    if '*' in wildcard:
        if wildcard in wildcard_replacements:
            wildcard_replacement = wildcard_replacements[wildcard]
    return wildcard_replacement


def list_apache_domains(vhosts: List[ApacheVhost]):
    """ Parse Apache vhosts in search of domains.
    Return a list of ApacheSource.
    """
    print_debug('Listing Apache domains.')
    apache_conf = load_apache_conf(vhosts)

    sources = []
    for vhost in vhosts:
        domains = list(set(vhost.aliases + [vhost.servername]))  # list(set(list)) removes duplicates

        for domain in domains:
            # Search if there is an ApacheSource for this domain/path pair
            source = None
            for s in sources:
                if s.domain == domain and s.path == vhost.path:
                    source = s
                    break
            # Else, create a new ApacheSource
            if not source:
                roommates = list(set(domains) - { domain })  # substraction can only be done on sets, not lists
                source = ApacheSource(domain, roommates, vhost.path)
                sources.append(source)

            source.add_port(vhost.port)

            # Agregate other infos from vhost conf lines
            for file_path, file_conf in apache_conf[vhost.path].items(): # apache_conf[vhost.path] contains a dict of { file_path: { nline, line } }
                for nline, line in file_conf.items():
                    words = line.split()
                    if ('ServerName' in words or 'ServerAlias' in words) and domain in words:
                        source.add_line_number(nline)
                    if 'SSLCertificateFile' in words:
                        source.add_certificate(words[1])
                    if 'SSLCertificateChainFile' in words:
                        source.add_intermediate_certificate(words[1])
    return sources


def parse_apache_dump_vhosts():
    """ Parse Apache DUMP_VHOSTS in search of domains.
    Return a dict of { domain: ApacheVhost object }
    """
    print_debug('Parsing output of apache2ctl -t -D DUMP_VHOSTS.')
    try:
        stdout, stderr, rc = execute('apache2ctl -t -D DUMP_VHOSTS')
    except:
        print_debug('Apache is not present, passing.')
        return {}

    vhosts = []
    cur_vhost = None
    for line in stdout:
        words = line.strip().split()
        if 'namevhost' in words and len(words) >= 5:
            # Save previous vhost
            if cur_vhost:
                vhosts.append(cur_vhost)
            # Parse vhost line
            # format: port PORT namevhost DOMAIN (VHOST_PATH:VHOST_LINE_NUMBER)
            port = int(words[1])
            domain = words[3].strip()
            path, nline = words[4].strip('()').split(':')
            cur_vhost = ApacheVhost(domain, path, int(port), int(nline))

        elif 'alias' in words and len(words) >= 2:
            # Parse alias line
            # format: alias DOMAIN
            cur_vhost.aliases.append(words[1].strip())

    return vhosts


def load_apache_conf(vhosts: List[ApacheVhost]):
    """ If available, parse Apache DUMP_CONFIG, else parse vhost files.
    vhosts: list of ApacheVhost objects
    Parse each vhost conf path, which can contain includes.
    Return conf lines by file in a dict:
        { conf_path: dict { included_file_path: dict { num_line: line } }
    Example: {
        "/etc/apache2/sites-enabled/default": {
            "/etc/apache2/sites-enabled/000-default": {
                "1": "<VirtualHost *:80>"
                "2": "ServerName default"
                "3": "Include /etc/apache2/ssl/000-default"
                …
            },
            "/etc/apache2/ssl/000-default": {
                "1": "SSLEngine on"
                "2": "SSLCertificateFile /etc/ssl/certs/evoadmin.test-www00.evolix.eu.crt"
                …
            },
        },
        "/etc/apache2/sites-enabled/mysite": {
        …
    }
    """
    conf_paths = list(set(map(operator.attrgetter('path'), vhosts))) # list(set(list)) removes duplicates
    apache_conf = {}
    if is_apache_mod_info:
        # Use mod_info instead of reading the conf file
        apache_conf = parse_apache_mod_info(conf_paths)
    else:
        # mod_info is not enabled, read the conf files
        for conf_path in conf_paths:
            apache_conf[conf_path] = load_apache_conf_file(conf_path)
    #pprint.pprint(apache_conf)
    return apache_conf


def parse_apache_mod_info(vhosts_paths: List[str]):
    """ Parse Apache DUMP_CONFIG of mod_info.
    Return conf lines by file in a dict:
        { vhost_path: dict { included_file_path: dict { num_line: line } }
    Example: see the one in load_apache_conf() docstring.
    """
    print_debug('Parsing output of apache2ctl -t -D DUMP_CONFIG.')
    stdout, stderr, rc = execute('apache2ctl -t -D DUMP_CONFIG')

    apache_conf = {}
    cur_vhost, cur_incl_file = None, None
    i, nline = 0, 0
    while i < len(stdout):
        line = stdout[i]
        words = line.split()

        if 'In file:' in line:
            cur_incl_file = None
            if os.path.exists(words[3]):
                if words[3] in vhosts_paths:
                    # File is a vhost
                    cur_vhost = words[3]
                    cur_incl_file = words[3]
                    if cur_vhost not in apache_conf:
                        apache_conf[cur_vhost] = {}
                    if cur_incl_file not in apache_conf[cur_vhost]:
                        apache_conf[cur_vhost][cur_incl_file] = {}

                elif words[3] not in vhosts_paths:
                    # File is not a vhost but can be useful (for example an included file)
                    cur_incl_file = words[3]
                    if cur_vhost and (cur_vhost in vhosts_paths) and (cur_incl_file not in apache_conf[cur_vhost]):
                        apache_conf[cur_vhost][cur_incl_file] = {}
                    i += 1
                    continue

        elif cur_vhost:
            if words[0] == '#' and words[1].rstrip(':').isdigit():  # format: ' #  N:'
                # This line indicates the line number, the next one is the real conf line
                nline = int(words[1].rstrip(':'))
                if i+1 > len(stdout)-1: break # EOF (just in case, but it should never happen)
                nextline = stdout[i+1]
                if cur_vhost in vhosts_paths:
                    apache_conf[cur_vhost][cur_incl_file][nline] = nextline
                i += 1
            else:
                nline += 1  # we don't know line number, so we guess it by incrementing the previous number
                if cur_vhost in vhosts_paths:
                    apache_conf[cur_vhost][cur_incl_file][nline] = line
        i += 1
    return apache_conf


def load_apache_conf_file(path):
    """ Recursively parse Apache configuration.
    Return conf lines by file (for Includes)
    in a dict { file_path: dict { num_line: line }
    """
    conf_files = { path: {} }
    with open(path, encoding='utf-8') as f:
        nline = 0
        for line in f:
            nline += 1
            line = strip_comments(line).strip()
            if not line: continue
            words = line.split()
            conf_files[path][nline] = line
            if 'Include' in words or 'IncludeOptional' in words:
                included_path_glob = words[1] # globbing can be used in Include directive
                included_paths = glob.glob(included_path_glob)
                for included_path in included_paths:
                    conf_files.update(load_apache_conf_file(included_path))
    return conf_files


def list_nginx_domains():
    """ Parse Nginx dynamic conf in search of domains.
    Return a list of NginxSource.
    """
    print_debug('Listing Nginx domains.')
    conf = parse_nginx_T()
    sources = []
    for file_path in conf:
        if not file_path.startswith('/etc/nginx/sites-enabled'):
            continue

        # Parse conf in file_path
        file_conf = flatten_nginx_conf_file(file_path, conf)
        conf_file_domains = {}  # { 'example.com': { 'ports': [], 'line_numbers': [] } }

        # For each server directive, all domains, ports and other relevant infos are completely parsed first, then added to a NginxSource.
        # This is because
        server_domains_line_numbers = {}  # { 'example.com': [line_number_1, line_number_2…] }
        server_ports = []
        for sub_conf_path, nline, line in file_conf:
            # Parse server {} blocks
            if line.startswith('server') and line.endswith('{'): # format: server {
                line = line.strip(' {')
                if line == 'server':
                    # New vhost, save previous vhost infos in conf_file_domains and reset the variables
                    for domain, line_numbers in server_domains_line_numbers.items():
                        if domain not in conf_file_domains:
                            conf_file_domains[domain] = { 'ports': [], 'line_numbers': [] }
                        conf_file_domains[domain]['line_numbers'].extend(line_numbers)
                        conf_file_domains[domain]['ports'].extend(server_ports)
                    server_domains_line_numbers = {}
                    server_ports = []

            # Parse line and note relevant infos (port, domain…)
            elif line.startswith('listen') or line.startswith('server_name'):
                words = line.strip('; ').split()
                if words[0] == 'listen':
                    server_ports.extend(parse_nginx_server_port(words))
                elif words[0] == 'server_name':
                    domains = parse_nginx_server_names(words)
                    for domain in domains:
                        if domain not in server_domains_line_numbers:
                            server_domains_line_numbers[domain] = []
                        server_domains_line_numbers[domain].append(nline)

        # EOF, save last server {} infos in conf_file_domains
        for domain, line_numbers in server_domains_line_numbers.items():
            if domain not in conf_file_domains:
                conf_file_domains[domain] = { 'ports': [], 'line_numbers': [] }
            conf_file_domains[domain]['line_numbers'].extend(line_numbers)
            conf_file_domains[domain]['ports'].extend(server_ports)

        # Then, create a NginxSource for each domain found in conf file
        for domain in conf_file_domains:
            roommates = list(set(conf_file_domains) - { domain }) # list(set(list)) removes duplicates
            source = NginxSource(domain, roommates, file_path)
            for port in conf_file_domains[domain]['ports']:
                source.add_port(port)
            for line_number in conf_file_domains[domain]['line_numbers']:
                source.add_line_number(line_number)
            if source not in sources:
                sources.append(source)
    return sources


def parse_nginx_T():
    """ Parse configuration files in `nginx -T` output.
    Return conf lines by file in a dict: { file_path: (line_number, line) }
    """
    try:
        stdout, stderr, rc = execute('nginx -T')
    except:
        print_debug('Nginx is not present, passing.')
        return {}

    conf = {}
    line_number = 0
    cur_conf_path = None
    for line in stdout:
        line_number += 1
        if line.startswith('# configuration file '):  # format: # configuration file <PATH>:
            line_number = 0
            words = line.strip(' ;').split()
            cur_conf_path = words[3].strip(' :')
            conf[cur_conf_path] = []
        else:
            line = strip_comments(line).strip()
            if line:
                conf[cur_conf_path].append((line_number, line))
    return conf


def flatten_nginx_conf_file(file_path: str, conf: Dict[str, Tuple[int, str]]):
    """ Parse file_path and included files recursively
    Return a List of tuple (path, line_number, line).
    conf: previously parsed Nginx conf
    """
    lines = []
    for nline, line in conf[file_path]:
        words = line.split()
        if words[0] == 'include':
            include_glob = words[1].strip(';')
            include_paths = glob.glob(include_glob)
            for include_path in include_paths:
                lines.extend(flatten_nginx_conf_file(include_path, conf))
        else:
            lines.append((file_path, nline, line))
    return lines


def parse_nginx_server_port(words: List[str]):
    """ Return a list of ports found in words.
    """
    # line format: [IP:]<PORT> [[IP:]<PORT>...] | <OTHER_DIRECTIVES>
    ports = []
    for ip_port in words[1:]:
        parts = ip_port.split(':')
        for part in parts:
            try:
                port = int(part)
                if part not in ports:
                    ports.append(port)
            except: # Not a port
                continue
    return ports


def parse_nginx_server_names(words: List[str]):
    """ Return a list of server_names found in words.
    """
    # line format: server_name <DOMAIN1> [<DOMAINS2 ...];
    domains = []
    for d in words[1:]:
        domain = d.strip()
        if domain in ['_', 'localhost', 'munin']:
            continue
        domains.append(domain)
    return domains


def list_vhost_domains(site_enabled_name: str):
    """ Find if site_enabled_name has a corresponding /etc/apache2/site-enabled/NAME.conf.
    Return its domains as a list of DomainSource.
    """
    matching_vhosts = []
    for vhost in parse_apache_dump_vhosts():
        name = os.path.basename(vhost.path)
        if name.endswith('.conf') or name.endswith('.cfg'):
            name = '.'.join(name.split('.')[:-1])  # remove extension
        if name == site_enabled_name:
            matching_vhosts.append(vhost)

    sources = []
    if matching_vhosts:
        sources = list_apache_domains(matching_vhosts)

    # TODO Nginx

    return sources


def list_certificates_domains(dir_path: str, source: str):
    """ Parse certificates in dir_path in search of domains (not recursive).
    Warning: list only certs in dir_path, no recursive search.
    Return a list of CertificateSource."
    """
    print_debug('Listing {} certificates domains for source {}.'.format(dir_path, source))
    if not os.path.exists(dir_path):
        print_debug('Directory {} does not exist, passing.'.format(dir_path))
        return []

    return list_tls_domains_of(dir_path, source)


def list_letsencrypt_domains():
    """ Parse certificates in /etc/letsencrypt in search of domains.
    Return a list of CertificateSource.
    """
    print_debug('Listing Let\'s Encrypt certificates domains.')
    if not shutil.which('certbot'):
        print_debug('Certbot not installed, passing.')
        return []

    if shutil.which('evoacme'):
        return list_tls_domains_of('/etc/letsencrypt', 'evoacme', '/etc/letsencrypt/{}/live/cert.crt')
    else:
        return list_tls_domains_of('/etc/letsencrypt/live', 'certbot', '/etc/letsencrypt/live/{}/cert.pem')


def list_tls_domains_of(base_path: str, source_type: str, cert_path_template: str = None):
    """ Parse certificates in directory base_path in search of domains.
    cert_path_template:
        Certificate path containing a {} substitution for String.format() (optional).
        The variable passed to format() is each file or directory found in base_path.
        Exemple for Let's Encrypt:
            base_path='/etc/letsencrypt/live'
            cert_path_template='/etc/letsencrypt/live/{}/cert.pem'
    Return a list of CertificateSource.
    """
    base_path = base_path.rstrip('/')
    if not os.path.exists(base_path):
        print_debug('Directory {} does not exist, passing.'.format(base_path))
        return []

    sources = []
    for item in os.listdir(base_path):
        if cert_path_template:
            cert_path = cert_path_template.format(item)
        else:
            cert_path = os.path.join(base_path, item)
        if os.path.isfile(cert_path):
            if base_path == '/etc/ssl/certs' and os.path.islink(cert_path):
                continue  # if file is a link, it is a CA cert, ignore it
            cert_sources = get_certificate_domains(cert_path, source_type)
            if cert_sources:
                sources.extend(cert_sources)
    return sources


def get_certificate_domains(cert_path: str, source_type: str):
    """ List the domains in the certificate (in X509 PEM format).
    Return a list of CertificateSource.
    """
    cert = load_certificate(cert_path)
    if not cert: return []
    all_domains = list(set(cert.cns + cert.sans))  # list(set(list)) removes duplicates

    sources = []
    for domain in all_domains:
        roommates = list(set(all_domains) - {domain})
        source = CertificateSource(domain, roommates, source_type, cert_path)
        if domain in cert.cns:
            source.add_cert_attribute('CN')
        if domain in cert.sans:
            source.add_cert_attribute('SAN')
        sources.append(source)

    return sources



"""
Business logic : DNS checks
"""


class DigThread(threading.Thread):
    """ Thread object that executes a dig and store its result.
    """
    def __init__(self, domain: str):
        threading.Thread.__init__(self, daemon=True)
        self.domain = domain
        self.exception = ''
        self.ips = {}  # dict { ip: reverse }

    def run(self):
        """Resolve domain with dig.
        """
        try:
            ips = dig(self.domain)

            if not ips:
                return  # no need for exception, empty self.ips will result in a specific DNSCheckStatus

            for ip in ips:
                if ip not in self.ips:
                    if is_numeric:
                        self.ips[ip] = ''
                    else:
                        reverse = dig(ip, get_reverse=True)
                        if not reverse:
                            reverse = ''
                        self.ips[ip] = reverse

        except Exception as e:
            self.exception = e


def check_domains(domains_summaries: Dict[str, DomainSummary]):
    """ Check resolution of domains and save it in a DNSCheckResult object
    in DomainSummary attribute dns_check_result.
    """
    # Run digs on all domains in threads
    jobs = []  # list of tuples (domain_summary, job)
    for domain, domain_summary in domains_summaries.items():
        if domain_summary.replacement_domain:
            domain = domain_summary.replacement_domain
        if '*' in domain:
            continue
        t = DigThread(domain)
        t.start()
        jobs.append((domain_summary, t))

    # Wait for jobs to finish or timeout
    for (domain_summary, job) in jobs:
        job.join(DNS_timeout)

        check_result = DNSCheckResult()
        if job.is_alive():
            check_result.set_status(DNSCheckStatus.DNS_TIMEOUT)
        domain_summary.set_dns_check_result(check_result)

    # Analyze DNS check results
    for (domain_summary, job) in jobs:
        check_result = domain_summary.dns_check_result

        if domain_summary.replacement_domain:
            check_result.add_comment('{} DNS checked on {}'.format(domain_summary.domain, domain_summary.replacement_domain))

        for ip in job.ips:
            check_result.add_ip(ip, ip in allowed_ips, job.ips[ip])

        local_ips = []
        for ip in job.ips:
            ip_object = ipaddress.ip_address(ip)
            if ip_object.is_private:
                local_ips.append(ip)
        if local_ips:
            check_result.add_comment('warning: domain resolves to local IP(s): {}'.format(', '.join(local_ips)))

        # Set check_result status
        if not check_result.status:
            if job.exception:
                check_result.set_status(DNSCheckStatus.ERROR)
                exception_name = job.exception.__class__.__name__
                check_result.add_comment('error: {}: {}'.format(exception_name, str(job.exception)))
                print_debug('Exception occured during DNS resolution of {}: {}: {}'.format(domain_summary.domain, exception_name, str(job.exception)))
            elif not job.ips:
                check_result.set_status(DNSCheckStatus.NO_DNS_RECORD)
            elif check_result.unknown_ips:
                check_result.set_status(DNSCheckStatus.UNKNOWN_IPS)
            else:
                check_result.set_status(DNSCheckStatus.OK)

    for domain, domain_summary in domains_summaries.items():
        # Set OK status if domain is in ignored_domains list
        if domain in ignored_domains:
            domain_summary.dns_check_result.set_status(DNSCheckStatus.OK)
            domain_summary.dns_check_result.add_comment('domain is in ignored domains list')

        # Set UNCHECKED status if wildcard has no replacement_domain
        elif '*' in domain and not domain_summary.replacement_domain:
            check_result = DNSCheckResult()
            check_result.set_status(DNSCheckStatus.UNCHECKED)
            check_result.add_comment('not checked because no replacement domain is configured')
            domain_summary.set_dns_check_result(check_result)

        # Add a comment if roommates have different DNS records
        check_result = domain_summary.dns_check_result
        for source in domain_summary.sources:
            if check_result.status != DNSCheckStatus.UNCHECKED:
                for roommate in source.roommates:
                    roommate_summary = domains_summaries[roommate]
                    if roommate_summary.dns_check_result != check_result:
                        check_result.add_comment('warning: roommate {} from source {} have different DNS records'.format(roommate, source.path))


def check_certificates(domains_summaries: Dict[str, DomainSummary]):
    """ Check certificates contained in domains_summaries.
    Return a list of Certificate objects with cert_check_result attribute
    containing a CertCheckResult object.
    """
    certs = {}
    for domain, domain_summary in domains_summaries.items():
        for source in domain_summary.sources:
            if isinstance(source, CertificateSource):
                if source.path not in certs:
                    certs[source.path] = check_certificate(source.path)

            if isinstance(source, WebSource):
                for cert_path in source.certificates:
                    if cert_path not in certs:
                        certs[cert_path] = check_certificate(cert_path)
                        certs[cert_path].vhost_path = source.path

    return list(certs.values())


def check_certificate(cert_path: str):
    """ Check certificate.
    Set a CertCheckResult object in certificate attribute cert_check_result
    and return Certificate object.
    """
    cert = load_certificate(cert_path)

    # Check expiration status
    warn_time = datetime.now() + delta_warn_cert_expiration
    if cert.end_date < datetime.now():
        check_result = CertCheckResult(CertExpirationStatus.EXPIRED)
    elif cert.end_date < warn_time:
        check_result = CertCheckResult(CertExpirationStatus.EXPIRES_SOON)
        expiration_delta = cert.end_date - datetime.now().replace(microsecond=0)
        check_result.add_comment('will expire in {}'.format(expiration_delta))
    else:
        check_result = CertCheckResult(CertExpirationStatus.OK)

    # [WIP] Check validity and chain
    if cryptography_handles_verification: # cryptography module version >= 42
        # Load certs to verify (including intermediates)
        with open(cert_path, 'rb') as cert_file:
           certs = x509.load_pem_x509_certificates(cert_file.read())  # crypto version >= 39
        cert = certs[0]
        intermediates = certs[1:] if len(certs) > 1 else None

        verifier_builder = x509.verification.PolicyBuilder()

        # Load root certs
        with open('/etc/ssl/certs/ca-certificates.crt', 'rb') as ca_bundle:
           store = x509.verification.Store(x509.load_pem_x509_certificates(ca_bundle.read()))
        verifier_builder = verifier_builder.store(store)

        verifier_builder = verifier_builder.time(verification_time)

        domain_name = x509.DNSName(cert.cns[0])
        verifier = verifier_builder.build_server_verifier(domain_name)
        chain = verifier.verify(cert, intermediates)
        # TODO: add to check result

    cert.set_cert_check_result(check_result)
    return cert


def load_certificate(cert_path: str):
    """ Parse certificate and return an instance of Certificate.
    """
    cn_domains = []
    san_domains = []
    end_date = None

    with open(cert_path, 'rb') as f:
        cert = x509.load_pem_x509_certificate(f.read(), default_backend())

        # Common name (format: subject: [...], CN=<COMMON NAME>, [...])
        cert_cns = cert.subject.get_attributes_for_oid(x509.oid.NameOID.COMMON_NAME)
        for cert_cn in cert_cns:
            dom = cert_cn.value
            match = domain_regex.search(dom)
            if match and match.group(1):
                cn_domains.append(match.group(1))
        if not cn_domains:
            return  # No CN containing a domain, ignore this certificate file

        # Subject Alternative Name (format: DNS:<DOMAIN1>, DNS:<DOMAIN2>[, ...])
        try:
            san_ext = cert.extensions.get_extension_for_oid(x509.oid.ExtensionOID.SUBJECT_ALTERNATIVE_NAME)
            cert_sans = san_ext.value.get_values_for_type(x509.DNSName)
            for cert_san in cert_sans:
                match = domain_regex.search(cert_san)
                if match and match.group(1):
                    if match.group(1) not in cn_domains: # avoid duplicates between CNs and SANs
                        san_domains.append(match.group(1))
                else:
                    print_warning('Could not decode SAN {} in {}'.format(cert_san, cert_path))
        except x509.ExtensionNotFound:
            pass

        # End date
        end_date = cert.not_valid_after_utc if cryptography_major_version >= 42 else cert.not_valid_after

    return Certificate(cert_path, cn_domains, san_domains, end_date)



"""
CLI : Print functions and classes
"""


class Color:
    """Color strings with shell color codes.
    Methods are static, which involves that they must be called on the class, not on an instance of the class.
    """

    def green(string: str):
        if is_interactive:
            string = '\033[0;32m' + string + '\033[0m'
        return string

    def bold_yellow(string: str):
        if is_interactive:
            string = '\033[1;33m' + string + '\033[0m'
        return string

    def build_colored_string(strings: List[str], color, separator: str = ','):
        """ Return a colored string joined with separator.
        When printing a colored string containing \n, tabulate has a bug that
        shifts left the next column item. Coloring each string and joining them
        later with join() is a workaround.
        Note: the separator is not colored.
        """
        colored_strings = map(color, strings)
        return separator.join(colored_strings)


def print_error_and_exit(s: str):
    if output == 'nrpe':
        print('UNKNOWN - {}'.format(s), file=sys.stderr, flush=True)
        sys.exit(3)
    else:
        print('Error: {}'.format(s), file=sys.stderr, flush=True)
        sys.exit(1)


def print_warning(s: str):
    if is_warning and output != 'nrpe':  # not NRPE because is_warning is true by default
        print('Warning: {}'.format(s), file=sys.stderr, flush=True)


def print_debug(s: str):
    if is_debug:
        print('Debug: {}'.format(s), flush=True)

def print_pager(output: str):
    """ Print output to pager.
    Less exits if the content fits into the current screen.
    This is why print() must also be called, in addition to this function.
    """
    if is_pager and is_interactive:
        tmp_stdout = tempfile.NamedTemporaryFile('w')
        print(output, file=tmp_stdout, flush=True)

        pager_cmd = '{} {}'.format(pager, tmp_stdout.name)
        subprocess.run(pager_cmd.split(), stdin=subprocess.PIPE)

        #tmp_stdout.close()

        # Before version 530, less do not print anything if screen is large enougth
        # with --quit-if-one-screen option. This is a workaround.
        #if less_version and less_version < 530: # bug encore présent en version 551
        print(output)
    else:
        print(output)


def print_allowed_ips():
    """ Print IPs allowed in configuration file allowed_ips_check.list.
    """
    print('Allowed IPs (from server IPs and evodomains configuration):')
    if not is_numeric:
        for ip in allowed_ip_reverses:
            print('  - {} ({})'.format(ip, allowed_ip_reverses[ip]))
    else:
        for ip in allowed_ips:
            print('  - {}'.format(ip))


def print_domains_table(domains_summaries: Dict[str, DomainSummary]):
    """ Print domains_summaries dict to stdout in a table.
    """
    header = ['Domain', 'Source', 'Path', 'Line number(s)', 'Certificate(s)']

    data = []
    for domain in sorted_domains(domains_summaries):
        for source in domains_summaries[domain].sources:
            if isinstance(source, WebSource):
                data.append([domain, source.source, source.path, ','.join(to_str(source.line_numbers)), ' '.join(source.certificates)])
            elif isinstance(source, EvodomainConfFileSource):
                data.append([domain, source.source + '_conf', source.path, ','.join(to_str(source.line_numbers)), ''])
            elif isinstance(source, EvodomainCommandLineSource):
                data.append([domain, source.source + 'CLI', '', '', ''])
            elif isinstance(source, CertificateSource):
                data.append([domain, source.source, '', '', source.path])
            else:
                data.append([domain, 'unknown'])

    output_str = tabulate(data, headers=header) # colalign parameter (idealy right for domain name) only available from Debian 11 (tabulate 0.8.7)
    print_pager(output_str)


def print_domains_json(domains_summaries: Dict[str, DomainSummary]):
    """ Print domains_summaries dict to stdout in JSON format."""
    print(json.dumps(domains_summaries, sort_keys=True, indent=4, cls=CustomJSONEncoder))


def print_dns_check_table(domains_summaries: Dict[str, DomainSummary]):
    """ Print DNS check results contained in domains_summaries dict to stdout in table format.
    """
    if not is_numeric:
        header = ['Domain', 'Check result', 'Allowed IPs (IP:reverse)', 'Unknown IPs (IP:reverse)', 'Comments']
    else:
        header = ['Domain', 'Check result', 'Allowed IPs', 'Unknown IPs', 'Comments']

    data = []
    for domain in sorted_domains_by_dns_check_result(domains_summaries):
        check_result = domains_summaries[domain].dns_check_result

        domain_str = domain
        check_result_str = check_result.status.name

        # When printing a colored string containing the '\n' character,
        # tabulate has a bug that shifts left the next column item.
        # Coloring each string and adding '\n' later (with join()) is a workaround.
        if not is_numeric:
            allowed_ips = [ ':'.join(filter(None, [ip, reverse])) for ip, reverse in check_result.allowed_ips.items() ]
            unknown_ips = [ ':'.join(filter(None, [ip, reverse])) for ip, reverse in check_result.unknown_ips.items() ]
            sep = ',\n'
        else:
            allowed_ips = check_result.allowed_ips.keys()
            unknown_ips = check_result.unknown_ips.keys()
            sep = ','

        # Add colors
        allowed_ips_str = Color.build_colored_string(allowed_ips, Color.green, sep)
        unknown_ips_str = Color.build_colored_string(unknown_ips, Color.bold_yellow, sep)
        if check_result.status == DNSCheckStatus.OK:
            domain_str = Color.green(domain_str)
            check_result_str = Color.green(check_result_str)
        else:
            domain_str = Color.bold_yellow(domain_str)
            check_result_str = Color.bold_yellow(check_result_str)
        colored_comments = []
        for comment in check_result.comments:
            if 'warning' in comment.lower():
                comment_color = Color.bold_yellow
            elif check_result.status == DNSCheckStatus.OK:
                comment_color = Color.green
            else:
                comment_color = Color.bold_yellow
            colored_comments.append(comment_color(comment))
        comments_str = '\n'.join(colored_comments)

        data.append([domain_str, check_result_str, allowed_ips_str, unknown_ips_str, comments_str])

    output_str = tabulate(data, headers=header) # colalign parameter (idealy right for domain name) only available from Debian 11 (tabulate 0.8.7)
    print_pager(output_str)


def print_dns_check_json(domains_summaries: Dict[str, DomainSummary]):
    """ Print DNS check results contained in domains_summaries dict to stdout in JSON format.
    """
    check_results = {}
    for domain in domains_summaries:
        check_results[domain] = domains_summaries[domain].dns_check_result

    print(json.dumps(check_results, sort_keys=True, indent=4, cls=CustomJSONEncoder))


def print_dns_check_nrpe(domains_summaries: Dict[str, DomainSummary]):
    """ Print DNS check results contained in domains_summaries dict to stdout in NRPE format.
    For now, output alerts only as WARNINGS.
    """
    # Count check results
    n_ok, n_warnings, n_errors = 0, 0, 0
    for domain in sorted_domains_by_dns_check_result(domains_summaries):
        check_result = domains_summaries[domain].dns_check_result
        if check_result.status == DNSCheckStatus.OK:
            n_ok += 1
        if check_result.status in [DNSCheckStatus.DNS_TIMEOUT, DNSCheckStatus.NO_DNS_RECORD, DNSCheckStatus.UNKNOWN_IPS]:
            n_warnings += 1
        if check_result.status == DNSCheckStatus.ERROR:
            n_errors += 1

    msg = 'WARNING' if n_warnings or n_errors else 'OK'
    print('{} - {} UNK / 0 CRIT / {} WARN / {} OK \n'.format(msg, n_errors, n_warnings, n_ok))

    for domain in sorted_domains_by_dns_check_result(domains_summaries):
        check_result = domains_summaries[domain].dns_check_result
        comments = ''
        if check_result.comments:
            comments = ' (' + ', '.join(check_result.comments).lower() + ')'

        if check_result.status == DNSCheckStatus.ERROR:
            print('UNKNOWN - DNS status of {}{}'.format(domain, comments))
        if check_result.status == DNSCheckStatus.DNS_TIMEOUT:
            print('WARNING - timeout resolving {}{}'.format(domain, comments))
        if check_result.status == DNSCheckStatus.NO_DNS_RECORD:
            print('WARNING - no DNS record for {}{}'.format(domain, comments))
        if check_result.status == DNSCheckStatus.UNKNOWN_IPS:
            if not is_numeric:
                allowed_ips = [ ':'.join(filter(None, [ip, reverse])) for ip, reverse in check_result.allowed_ips.items() ]
                unknown_ips = [ ':'.join(filter(None, [ip, reverse])) for ip, reverse in check_result.unknown_ips.items() ]
            else:
                allowed_ips = check_result.allowed_ips.keys()
                unknown_ips = check_result.unknown_ips.keys()
            unknown_ips = ', '.join(unknown_ips)
            allowed_ips = ', '.join(allowed_ips)
            if allowed_ips:
                print('WARNING - {} resolves to unknown IP(s): {} and allowed IPs:{}{}'.format(domain, unknown_ips, allowed_ips, comments))
            else:
                print('WARNING - {} resolves to unknown IP(s): {}{}'.format(domain, unknown_ips, comments))

    if is_verbose:
        for domain in sorted_domains_by_dns_check_result(domains_summaries):
            summary = domains_summaries[domain]
            if check_result.status == DNSCheckStatus.OK:
                comments = ''
                if check_result.comments:
                    comments = ' (' + ', '.join(check_result.comments).lower() + ')'

                if not is_numeric:
                    allowed_ips = [ ':'.join(filter(None, [ip, reverse])) for ip, reverse in check_result.allowed_ips.items() ]
                else:
                    allowed_ips = check_result.allowed_ips.keys()
                allowed_ips = ', '.join(allowed_ips)

                if allowed_ips:
                    print('OK - {} resolves to allowed IP(s): {}{}'.format(domain, allowed_ips, comments))
                else:  # case domain is in ignored domains list
                    print('OK - {}{}'.format(domain, comments))

    sys.exit(1) if n_warnings or n_errors else sys.exit(0)



def print_certificates_check_table(certificates: List[Certificate]):
    """ Print certificates check results to stdout in table format.
    """
    expired, expires_soon, ok = [], [], []
    for cert in certificates:
        status = cert.cert_check_result.expiration_status
        if status == CertExpirationStatus.EXPIRES_SOON:
            expires_soon.append(cert)
        elif status == CertExpirationStatus.EXPIRED:
            expired.append(cert)
        else:
            ok.append(cert)

    header = ['Path', 'Status', 'End date', 'Vhost', 'Common names', 'Alternate names',  'Comments']

    data = []
    for cert in sorted_certificates_by_path(expired) + sorted_certificates_by_path(expires_soon) + sorted_certificates_by_path(ok):
        status = cert.cert_check_result.expiration_status

        status_color, others_color = Color.green, Color.green
        if status == CertExpirationStatus.EXPIRES_SOON:
            status_color, others_color = Color.bold_yellow, Color.green
        elif status == CertExpirationStatus.EXPIRED:
            status_color, others_color = Color.bold_yellow, Color.bold_yellow

        path_str = others_color(cert.path)
        status_str = status_color(status.name)
        end_date_str = status_color(cert.end_date.strftime('%Y-%m-%d %H:%M'))
        vhost_str = status_color(cert.vhost_path) if cert.vhost_path else ''
        cns = Color.build_colored_string(cert.cns, others_color, ',')
        sans = Color.build_colored_string(cert.sans, others_color, '\n')
        comments_str = Color.build_colored_string(cert.cert_check_result.comments, others_color, '\n')

        data.append([path_str, status_str, end_date_str, vhost_str, cns, sans, comments_str])

    output_str = tabulate(data, headers=header)
    print_pager(output_str)


def print_certificates_check_json(certificates: List[Certificate]):
    """ Print certificates check results to stdout in JSON format.
    """
    certificates = sorted_certificates_by_path(certificates)
    print(json.dumps(certificates, indent=4, cls=CustomJSONEncoder))


def print_certificates_check_nrpe(certificates: List[Certificate]):
    """ Print certificates check results to stdout in NRPE format.
    Output CRITICAL alert if check status is EXPIRES_SOON or EXPIRED
    """
    expired, expires_soon, ok = [], [], []
    for cert in certificates:
        status = cert.cert_check_result.expiration_status
        if status == CertExpirationStatus.EXPIRES_SOON:
            expires_soon.append(cert)
        elif status == CertExpirationStatus.EXPIRED:
            expired.append(cert)
        else:
            ok.append(cert)

    msg = 'CRITICAL' if expires_soon or expired else 'OK'
    print('{} - {} CRIT / 0 WARN / {} OK\n'.format(msg, len(expired) + len(expires_soon), len(ok)))

    for cert in sorted_certificates_by_path(expired) + sorted_certificates_by_path(expires_soon) + sorted_certificates_by_path(ok):

        expiration_delta = cert.end_date - datetime.now().replace(microsecond=0)
        expiration_delta_str = str(expiration_delta) if expiration_delta <= delta_warn_cert_expiration else "{} days".format(expiration_delta.days)
        end_date_str = cert.end_date.strftime('%Y-%m-%d at %H:%M')
        vhost_str = ' Vhost: {}'.format(cert.vhost_path) if cert.vhost_path else ' Not used in a vhost.'

        status = cert.cert_check_result.expiration_status
        if status == CertExpirationStatus.EXPIRED:
            print('CRITICAL - {} ({}) is expired since {}!{}'.format(cert.path, cert.cns[0], end_date_str, vhost_str))
        elif status == CertExpirationStatus.EXPIRES_SOON:
            print('CRITICAL - {} ({}) will expire in {} ({}).{}'.format(cert.path, cert.cns[0], expiration_delta_str, end_date_str, vhost_str))
        elif status == CertExpirationStatus.OK and is_verbose:
            print('OK - {} ({}) will expire in {} ({}).'.format(cert.path, cert.cns[0], expiration_delta_str, end_date_str))

    sys.exit(1) if expired or expires_soon else sys.exit(0)



"""
Utilitary functions
"""


def execute(cmd: str, timeout: int = None, shell: bool = False):
    """ Execute shell command.
    - cmd: the command to execute
    - timeout: in seconds
    - shell: if True, pass directly the command to shell (useful for pipes).
    Before use shell=True, consider security warning:
      https://docs.python.org/3/library/kess.html#security-considerations

    Return stdout and stderr as arrays of UTF-8 strings, and the return code.
    """
    if not shell:
        cmd = cmd.split()
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=shell)
    stdout, stderr = proc.communicate(timeout=timeout)

    stdout_lines = stdout.decode('utf-8').splitlines()
    stderr_lines = stderr.decode('utf-8').splitlines()

    return stdout_lines, stderr_lines, proc.returncode


def dig(host: str, get_reverse: bool = False):
    """ Return a list of IPs or a domain if get_reverse is True.
    If reverse end with '.evolix.net' or '.rev.as197696.net', this part is removed.
    """
    if get_reverse:
        reverse = ''
        reverse_lookup = socket.getfqdn(host)
        match = domain_regex.search(reverse_lookup) #reverse_regex.search(reverse_lookup)
        if match:
            reverse = match.group(1)
            strip_domain_ends = ['.evolix.net', '.rev.as197696.net']
            for end in strip_domain_ends:
                if reverse.endswith(end):
                    reverse = reverse.rstrip(end)
            return reverse

    else:
        ips = []
        try:
            dns_lookup = socket.getaddrinfo(host, None, family=socket.AF_INET, proto=socket.IPPROTO_TCP)
        except socket.gaierror:
            pass  # no ips case will be handled downstream
        else:
            for result in dns_lookup:
                ip = result[4][0]
                match = ip_regex.search(ip)
                if match:
                    ip = match.group(1)
                    ips.append(ip)
        return ips


def query_ip_reverses(ips: List[str]):
    """ Query reverses and return a dict of { ip: reverse }.
    """
    reverses = {}
    for ip in ips:
        if ip == '127.0.0.1':
            reverses[ip] = 'localhost'
            continue

        # Use ThreadPool().apply_async() to set a timeout to the reverse lookup
        try:
            with multiprocessing.pool.ThreadPool() as pool:
                reverses[ip] = pool.apply_async(dig, (ip, True)).get(timeout=DNS_timeout)
        except multiprocessing.TimeoutError:
            pass

        if ip not in reverses or not reverses[ip]:
            reverses[ip] = 'reverse not found'

    return reverses


def strip_comments(string: str):
    """ Return string with any # comment removed."""
    return string.split('#')[0]


def to_str(data):
    """ Convert data to str.
    """
    if isinstance(data, list):
        return map(str, sorted(data))
    else:
        return str(data)

def get_main_domain(domain: str):
    """ Return main domain without subdomain.
    Example:
    - input 'www.example.com' will return 'example.com'.
    """
    splitted = domain.strip('.').split('.')[-2:]
    return '.'.join(splitted)


def get_sub_domain(domain: str):
    """ Return subdomain without main domain.
    Example:
    - input 'dev.www.example.com' will return 'dev.www'.
    """
    splitted = domain.strip('.').split('.')[:-2]
    return '.'.join(splitted)


def sorted_domains(domains_summaries: Dict[str, DomainSummary]):
    """ Returns the list sorted :
    1. root domain
    2. sub-domain (if there are multiple sub-domains, they are taken as a whole, see get_sub_domain())
    """
    s = sorted(domains_summaries, key = get_sub_domain)
    return sorted(s, key = get_main_domain)


def sorted_domains_by_dns_check_result(domains_summaries: Dict[str, DomainSummary]):
    """ Returns the list sorted :
    1: DNS check result
    2. root domain
    3. sub-domain (if there are multiple sub-domains, they are taken as a whole, see get_sub_domain())
    """
    s = sorted(domains_summaries, key = get_sub_domain)
    s = sorted(s, key = get_main_domain)
    return sorted(s, key = lambda domain: domains_summaries[domain].dns_check_result.status)


def sorted_certificates_by_path(certificates: List[Certificate]):
    """ Return Certificates sorted by path.
    """
    return sorted(certificates, key = lambda cert: cert.path)


def read_conf_file(file_path: str, regex = None): # regex: re.Pattern needs Python > 3.5
    """ Generic configuration reader.
    Strip empty lines and comments.
    If regex is provided, return a list of tuples. Each tuple contain the group matches.
    Example: [
        (group1, group2, …),
        …
    ]
    Else, return a list of striped lines of type string.
    """
    # Touch configuration file, in case of missing file
    open(file_path, 'a').close()

    cleaned_lines = []
    with open(file_path, encoding='utf-8') as f:
        for line in f:
            cleaned_line = strip_comments(line).strip()
            if cleaned_line:
                if regex:
                    match = regex.search(cleaned_line)
                    if match:
                        cleaned_lines.append(match.groups())
                    else:
                        print_warning('Malformed configuration line \'{}\' in {}.'.format(cleaned_line, file_path))
                else:
                    cleaned_lines.append(cleaned_line)
    return cleaned_lines





"""
HaProxy WIP, standby for now, maybe for version 2
"""


#def list_haproxy_acl_domains():
#    """ Parse HaProxy config file in search of domain ACLs or files containing list of domains.
#    Return a dict containing :
#    - key: HaProxy domains (from ACLs in /etc/haproxy/haproxy.cgf).
#    - value: a list of strings 'haproxy:/etc/haproxy/haproxy.cfg:<LINE_IN_CONF>'
#    """
#    print_debug('Listing HaProxy ACL domains')
#    domains = {}
#
#    if not os.path.isfile(haproxy_conf_path):
#        # HaProxy is not installed
#        print_warning('{} not found'.format(haproxy_conf_path))
#        return domains
#
#    # Domains from ACLs
#    with open(haproxy_conf_path, encoding='utf-8') as f:
#        line_number = 0
#        files = []
#        for line in f.readlines():
#            line_number += 1
#
#            # Handled line format:
#            #    acl <ACL_NAME> [hdr|hdr_reg|hdr_end](host) [-i] <STRING> [<STRING> [...]]
#            #    acl <ACL_NAME> [hdr|hdr_reg](host) [-i] -f <FILE>
#
#            line = strip_comments(line).strip()
#
#            if (not line) or (not line.startswith('acl')):
#                continue
#            if 'hdr(host)' not in line and 'hdr_reg(host)' not in line and 'hdr_end(host)' not in line:
#                continue
#
#            # Remove 'acl <ACL_NAME>' from line
#            line = ' '.join(line.split()[2:])
#
#            is_file = False
#            if ' -f ' in line:
#                is_file = True
#
#            # Limit: does not handle regex
#
#            words = line.split()
#            for word in line.split():
#                if word in ['hdr(host)', 'hdr_reg(host)', 'hdr_end(host)', '-f', '-i']:
#                    continue
#
#                if is_file:
#                    if word not in files:
#                        print('Found HaProxy domains file {}'.format(word))
#                        files.append(word)
#                else:
#                    dom_infos = 'haproxy:{}:{}'.format(haproxy_conf_path, line_number)
#                    if word not in domains:
#                        domains[word] = []
#                    if dom_infos not in domains[word]:
#                        domains[word].append(dom_infos)
#
#        for f in files:
#            domains_to_add = read_haproxy_domains_file(f, 'haproxy')
#            domains.update(domains_to_add)
#
##TODO remove (call elsewhere)
#    # Domains from HaProxy certificates
##    domains_to_add = list_haproxy_certs_domains()
##    domains.update(domains_to_add)
#
#    return domains
#
#
#def read_haproxy_domains_file(domains_file_path, source):
#    """ Process a file containing a list of domains :
#    - domains_file_path: path of the file to parse
#    - source: string keyword to prepend to the domains infos. Exemple: 'haproxy'
#    Return a dict containing :
#    - key: domain (from domains_file_path)
#    - value: a list of strings 'source:domains_file_path:<LINE_IN_BLOCK>'
#    """
#    domains = {}
#
#    try:
#        with open(domains_file_path, encoding='utf-8') as f:
#            line_number = 0
#            for line in f.readlines():
#                line_number += 1
#
#                dom = strip_comments(line).strip()
#                if not dom:
#                    continue
#
#                dom_infos = '{}:{}:{}'.format(source, domains_file_path, line_number)
#                if dom not in domains:
#                    domains[dom] = []
#                if dom_infos not in domains[dom]:
#                    domains[dom].append(dom_infos)
#
#    except FileNotFoundError as e:
#        print_warning('FileNotFound {}'.format(domains_file_path))
#        print_warning(e)
#
#    return domains


#def list_haproxy_certificates_domains():
#    """ Return the domains present in HaProxy SSL certificates.
#    Return a dict containing:
#    - key: domain (from domains_file_path)
#    - value: a list of strings 'haproxy_certs:cert_path:CN|SAN'
#    """
#    print_debug('Listing HaProxy certificates domains')
#
#    sources = []
#
#    # Is HaProxy installed?
#    if not os.path.isfile(haproxy_conf_path):
#        print_warning('{} not found'.format(haproxy_conf_path))
#        return sources
#
#    # Check if HaProxy version supports 'show ssl cert' command
#    supports_show_ssl_cert = does_haproxy_support_show_ssl_cert()
#
#    if supports_show_ssl_cert:
#        socket = get_haproxy_stats_socket()
#        # Ajoute l'IP locale dans le cas d'un port TCP (au lieu d'un socket Unix)
#        if socket.startswith(':'):
#            socket = 'tcp:127.0.0.1{}'.format(socket)
#
#        #print('echo "show ssl cert" | socat stdio {}'.format(socket))
#        stdout, stderr, rc = execute('echo "show ssl cert" | socat stdio {}'.format(socket), shell=True)
#
#        for cert_path in stdout:
#            if cert_path.strip().startswith('#'):
#                continue
#            if os.path.isfile(cert_path):
#                domains_to_add = get_certificate_domains(cert_path, 'haproxy_certs')
#                sources.extend(domains_to_add)
#
#    else:
#        # Get HaProxy certificates paths (can be directory or file)
#        # Line format : bind *:<PORT> ssl crt <CERT_PATH>
#        cert_paths = []
#        with open(haproxy_conf_path, encoding='utf-8') as f:
#            for line in f.readlines():
#                line = strip_comments(line).strip()
#                if not line: continue
#                if ' crt ' in line:
#                    crt_index = line.find(' crt ')
#                    subs = line[crt_index+5:]
#                    cert_path = subs.split(' ')[0]  # in case other options are after cert path
#                    cert_paths.append(cert_path)
#            print('hap certs', cert_paths)
#
#        for cert_path in cert_paths:
#            if os.path.isfile(cert_path):
#                print(cert_path)
#                domains_to_add = get_certificate_domains(cert_path, 'haproxy_certs')
#            elif os.path.isdir(cert_path):
#                domains_to_add = list_tls_domains_of(p, 'haproxy_certs')
#            sources.extend(domains_to_add)
#
#    return sources
#
#
#def does_haproxy_support_show_ssl_cert():
#    """ Return True if HaProxy version supports 'show ssl cert' command (version >= 2.2)."""
#
#    stdout, stderr, rc = execute('dpkg-query --show haproxy', shell=True)
#
#    supports_show_ssl_cert = False
#
#    if rc == 0:
#        for line in stdout:
#            # Line format: PACKAGE VERSION
#            words = line.strip().split()
#            if words[0] == 'haproxy':
#                major, minor = words[1].split('.')[:2]
#                if int(major) >= 2 and int(minor) >= 2:
#                    supports_show_ssl_cert = True
#
#    return supports_show_ssl_cert
#
#
#def get_haproxy_stats_socket():
#    """ Return HaProxy stats socket."""
#
#    with open(haproxy_conf_path, encoding='utf-8') as f:
#        line_number = 0
#        for line in f.readlines():
#            words = line.strip().split()
#            if len(words) >= 3 and words[0] == 'stats' and words[1] == 'socket':
#                return words[2]
#
#    return None


## Entry point
if __name__ == '__main__':
    program_name = os.path.splitext(os.path.basename(__file__))[0]
    main(sys.argv[1:])

